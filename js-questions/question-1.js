
async function infiniteLoopWithPause(callback, timeout) {
  while(true) {
    callback();
    await new Promise(resolve => setTimeout(resolve, timeout));
  }
}

infiniteLoopWithPause(() => {
  console.log('It works!');
}, 1000);
