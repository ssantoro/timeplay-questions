var assert = require('assert');

const reducer = require('./question-2.js');

describe('reducer', () => {
  it('should add a message', () => {
    let initialState = {
      messageList: []
    };
  
    let newState = reducer(initialState, {
      'type': 'add-message',
      'message': 'test message'
    });
  
    assert.ok(newState.messageList.includes('test message'));
  });

  it('should remove a message', () => {
    let initialState = {
      messageList: [
        'message 0',
        'message 1',
        'message 2'
      ]
    };
  
    let newState = reducer(initialState, {
      'type': 'remove-message',
      'id': 1
    });

    assert.equal(newState.messageList.includes('message 1'), false);
  });

  it('should throw an error on an invalid action', () => {
    let initialState = {
      messageList: []
    };

    try {
      reducer(initialState, {
        'type': 'invalid-action'
      });

      assert.ok(false);
    } catch {
      assert.ok(true);
    }

  })
});
