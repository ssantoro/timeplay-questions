const fetch = require('node-fetch');

let apis = [
  "https://i-dont-exist.com/api/v2",
  "https://httpstat.us/500/cors",
  "https://httpstat.us/404/cors",
  // returns the json {"code": 200, "description": "OK"}
  "https://httpstat.us/200/cors",
]

async function call_apis(urls) {
  // Build array of Promises
  let requests = urls.map(url => {
    return new Promise(async (resolve, reject) => {
      try {
        let response = await fetch(url, {
          headers: {
            'Accept': 'application/json',
          }
        });

        if (response.ok === true) {
          resolve(await response.json());
        } else {
          resolve(undefined);
        }
      } catch {
        resolve(undefined);
      }
    })
  });

  return await Promise.all(requests)
}

call_apis(apis).then((result_arr) => {
  console.log(result_arr)
});
