defmodule Question4.DataStore do
  use GenServer
  
  def start_link(initial_data \\ %{}) do
    GenServer.start_link(__MODULE__, initial_data)
  end

  # Client functions
  
  @doc """
    Adds the specified element to the map under the given key.
  """
  def put(pid, key, element) do
    GenServer.cast(pid, {:put, key, element})
  end

  @doc """
    Returns the value in the map, if it doesn't exist, resturns the default
    instead.
  """
  def get(pid, key, default \\ nil) do
    GenServer.call(pid, {:get, key, default})
  end

  @doc """
    Removes the element from the map, and returns the value associated with 
    the key, if the key doesn't exist, returns the value specified as default.
  """
  def pop(pid, key, default \\ nil) do
    GenServer.call(pid, {:pop, key, default})
  end

  @doc """
    Clears all the entries in the map.
  """
  def reset(pid) do
    GenServer.cast(pid, :reset)
  end

  # Server callbacks
  @impl true
  def init(initial) do
    {:ok, initial}
  end

  @impl true
  def handle_cast({:put, key, element}, state) do
    {:noreply, Map.put(state, key, element)}
  end

  @impl true
  def handle_cast(:reset, _state) do
    {:noreply, %{}}
  end

  @impl true
  def handle_call({:get, key, default}, _from, state) do
    {:reply, Map.get(state, key, default), state}
  end

  @impl true
  def handle_call({:pop, key, default}, _from, state) do

    {value, state} = Map.pop(state, key, default)

    {:reply, value, state}
  end
end
