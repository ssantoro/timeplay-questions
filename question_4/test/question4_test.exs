defmodule Question4Test do
  use ExUnit.Case
  doctest Question4

  setup do
    {:ok, pid} = Question4.DataStore.start_link()

    {:ok, pid: pid}
  end

  test "key and value can be put in the map", %{pid: pid} do
    Question4.DataStore.put(pid, "key", "value")

    assert Question4.DataStore.get(pid, "key") == "value"
  end

  test "value can be gotten from the map", %{pid: pid} do
    Question4.DataStore.put(pid, "key", "value")

    assert Question4.DataStore.get(pid, "key") == "value"
  end

  test "reset clears the map", %{pid: pid} do
    Question4.DataStore.put(pid, "key1", "value")
    Question4.DataStore.put(pid, "key2", "value")
    Question4.DataStore.put(pid, "key3", "value")
    Question4.DataStore.reset(pid)

    assert Question4.DataStore.get(pid, "key1") == nil
    assert Question4.DataStore.get(pid, "key2") == nil
    assert Question4.DataStore.get(pid, "key3") == nil
  end

  test "value can be popped from the map", %{pid: pid} do
    Question4.DataStore.put(pid, "test", "value")
    value = Question4.DataStore.pop(pid, "test", "value")

    assert value == "value"
    assert Question4.DataStore.get(pid, "test", "default") == "default"
  end

end
